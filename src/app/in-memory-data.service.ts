import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'Mr. Nice', img: 'assets/img/ferrari.jpg' },
      { id: 12, name: 'Narco', img: 'assets/img/ferrari.jpg' },
      { id: 13, name: 'Bombasto', img: 'assets/img/ferrari.jpg' },
      { id: 14, name: 'Celeritas', img: 'assets/img/ferrari.jpg' },
      { id: 15, name: 'Magneta', img: 'assets/img/ferrari.jpg' },
      { id: 16, name: 'RubberMan', img: 'assets/img/ferrari.jpg' },
      { id: 17, name: 'Dynama', img: 'assets/img/ferrari.jpg' },
      { id: 18, name: 'Dr IQ', img: 'assets/img/ferrari.jpg' },
      { id: 19, name: 'Magma', img: 'assets/img/ferrari.jpg' },
      { id: 20, name: 'Tornado', img: 'assets/img/ferrari.jpg' }
    ];
    return {heroes};
  }
}
